package com.example.george.chicagospeedcams;

import android.test.InstrumentationTestCase;

import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The purpose of these tests is to determine which values the json file
 * contains and what assumptions we can make when using the data.
 */
public class JsonTest extends InstrumentationTestCase {
    private static final String TAG = "JsonTest";
    private ArrayList<HashMap<String,String>> raw;
    private ArrayList<Camera> cameras;

    @Before
    public void setUp() {
        InputStream in = getInstrumentation().getTargetContext().getResources().openRawResource(R.raw.redlight);
        CameraParser parser = new CameraParser(in);
        raw = parser.getRawCameras();
        cameras = parser.getCameras();
    }

    @Test
    public void testApproachValues() {
        ArrayList<String> values = new ArrayList<String>();

        for (HashMap<String,String> r : raw) {
            if (! values.contains(r.get(CameraParser.APPROACH_1))) {
                values.add(r.get(CameraParser.APPROACH_1));
            }

            if (! values.contains(r.get(CameraParser.APPROACH_2))) {
                values.add(r.get(CameraParser.APPROACH_2));
            }

            if (! values.contains(r.get(CameraParser.APPROACH_3))) {
                values.add(r.get(CameraParser.APPROACH_3));
            }
        }

        assertEquals(9, values.size());

        assertTrue(values.contains("NB"));
        assertTrue(values.contains("SB"));
        assertTrue(values.contains("EB"));
        assertTrue(values.contains("WB"));
        assertTrue(values.contains("NEB"));
        assertTrue(values.contains("SWB"));

        assertTrue(values.contains("SP"));
        assertTrue(values.contains("null"));
        assertTrue(values.contains(""));
    }

    @Test
    public void testApproachValuesDistict() {
        for (HashMap<String,String> r : raw) {
            String approach1 = r.get(CameraParser.APPROACH_1);
            String approach2 = r.get(CameraParser.APPROACH_2);
            String approach3 = r.get(CameraParser.APPROACH_3);

            assertTrue(approach1 != approach2);
            assertTrue(approach1 != approach3);
            assertTrue(approach2 != approach3);
        }
    }

    @Test
    public void testSpeedCamOnlyApproach3() {
        for (HashMap<String,String> r : raw) {
            assertTrue(!r.get(CameraParser.APPROACH_1).equals("SP"));
            assertTrue(!r.get(CameraParser.APPROACH_2).equals("SP"));
        }
    }

    @Test
    public void testSpeedCamera() {
        for (HashMap<String, String> r : raw) {
            String intersection = r.get(CameraParser.INTERSECTION);
            String approach3 = r.get(CameraParser.APPROACH_3);

            if (intersection.contains("(Speed Camera)")) {
                assertTrue(approach3.equals("SP"));
            }

            if (approach3.equals("SP")) {
                assertTrue(intersection.contains("(Speed Camera"));
            }
        }
    }

    @Test
    public void testEmptyStringOnlyApproach2() {
        for (HashMap<String, String> r : raw) {
            assertTrue(!r.get(CameraParser.APPROACH_1).equals(""));
            assertTrue(!r.get(CameraParser.APPROACH_3).equals(""));
        }
    }

    @Test
    public void testIdUnique() {
        for (int i = 0; i < cameras.size(); i++) {
            for (int j = 0; j < cameras.size(); j++){
                if (i != j) {
                    assertTrue(cameras.get(i).getId() != cameras.get(j).getId());
                }
            }
        }
    }

    @Test
    public void testCamerasSameSizeAndOrder() {
        assertTrue(raw.size() == cameras.size());

        for (int i = 0; i < cameras.size(); i++) {
            assertTrue(cameras.get(i).getId() == Integer.parseInt(raw.get(i).get(CameraParser.ID)));
        }
    }

    @Test
    public void testRedLightCam() {
        int i = 0;
        for (i = 0; i < cameras.size(); i++) {
            // find index of the camera we're looking for
            if (cameras.get(i).getId() == 469) {
                break;
            }
        }

        Camera c = cameras.get(i);
        assertTrue(c.getLocation().getLatitude() == 41.76491);
        assertTrue(c.getLocation().getLongitude() == -87.663794);
        assertTrue(c.getIntersection().equals("Ashland-71st"));
        assertFalse(c.isSpeedCam());
    }

    @Test
    public void testSpeedCam() {
        int i = 0;
        for (i = 0; i < cameras.size(); i++) {
            // find index of the camera we're looking for
            if (cameras.get(i).getId() == 9122) {
                break;
            }
        }

        Camera c = cameras.get(i);
        assertTrue(c.getLocation().getLatitude() == 41.72075680558273);
        assertTrue(c.getLocation().getLongitude() == -87.53543678735923);
        assertTrue(c.getIntersection().equals("9618 S Ewing Ave"));
        assertTrue(c.isSpeedCam());
    }
}
