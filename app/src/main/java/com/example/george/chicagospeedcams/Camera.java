package com.example.george.chicagospeedcams;

import android.location.Location;

public class Camera {
    private Location location;
    private int id;
    private String intersection;
    private boolean isSpeedCam;

    public Camera(double lat, double lon, int id, String intersection, boolean isSpeedCam) {
        this.location = new Location("Camera");
        this.location.setLatitude(lat);
        this.location.setLongitude(lon);

        this.id = id;
        this.intersection = intersection;
        this.isSpeedCam = isSpeedCam;
    }

    public Location getLocation() {
        return location;
    }

    public int getId() {
        return id;
    }

    public String getIntersection() {
        return intersection;
    }

    public boolean isSpeedCam() {
        return isSpeedCam;
    }

    @Override
    public String toString() {
        return "Camera{" +
                "lat=" + location.getLatitude() +
                ", lon=" + location.getLongitude() +
                ", id=" + id +
                ", intersection='" + intersection + '\'' +
                ", isSpeedCam=" + isSpeedCam +
                '}';
    }
}
