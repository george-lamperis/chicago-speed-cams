package com.example.george.chicagospeedcams;

import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Generates Camera objects from JSON.
 */
public class CameraParser {
    public static final String ID = "intid";
    public static final String LATITUDE = "lat";
    public static final String LONGITUDE = "lon";
    public static final String INTERSECTION = "intersection";
    public static final String APPROACH_1 = "approach1";
    public static final String APPROACH_2 = "approach2";
    public static final String APPROACH_3 = "approach3";

    private static final String TAG = "CameraParser";

    private JsonReader reader;
    private ArrayList<HashMap<String,String>> raw;
    private ArrayList<Camera> cameras;

    /**
     * Constructs a CameraParser object using an InputStream to a json file
     * containing camera data.
     */
    public CameraParser(InputStream in) {
        reader = new JsonReader(new InputStreamReader(in));
        raw = new ArrayList<HashMap<String,String>>();
        cameras = new ArrayList<Camera>();

        try {
            reader.beginObject();
            reader.nextName();
            reader.beginArray();

            while (reader.hasNext()) {
                HashMap<String,String> c = parseRawCamera();
                raw.add(c);
            }

            reader.endArray();
            reader.endObject();
        } catch (IOException e) {
            e.printStackTrace();
        }

        createCameras();
    }

    /**
     * Returns a list of Camera objects generated from the json file.
     */
    public ArrayList<Camera> getCameras() {
        return new ArrayList<Camera>(cameras);
    }

    /**
     * Returns a list of HashMaps containing the raw name/value pairs from the
     * json file being parsed. The keys for this HashMap are stored as public
     * static variables in this class (see above). This is primarily for testing
     * the json file.
     */
    public ArrayList<HashMap<String,String>> getRawCameras() {
        return new ArrayList<HashMap<String,String>>(raw);
    }

    /**
     * Parses a single camera and returns it.
     */
    private HashMap<String,String> parseRawCamera() throws IOException {
        HashMap<String, String> hashMap = new HashMap<String,String>();
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            String value = reader.nextString().trim();  // trailing/leading whitespace

            if (name.equals(ID)) {
                hashMap.put(ID, value);
            } else if (name.equals(LATITUDE)) {
                hashMap.put(LATITUDE, value);
            } else if (name.equals(LONGITUDE)) {
                hashMap.put(LONGITUDE, value);
            } else if (name.equals(INTERSECTION)) {
                hashMap.put(INTERSECTION, value);
            } else if (name.equals(APPROACH_1)) {
                hashMap.put(APPROACH_1, value);
            } else if (name.equals(APPROACH_2)) {
                hashMap.put(APPROACH_2, value);
            } else if (name.equals(APPROACH_3)) {
                hashMap.put(APPROACH_3, value);
            } // else - the value is skipped
        }
        reader.endObject();

        return hashMap;
    }

    /**
     * Generates Camera objects from the raw data extracted from json.
     */
    private void createCameras() {
        for (HashMap<String,String> r : raw) {
            // These are switched on purpose, json file is wrong
            double latitude = Double.parseDouble(r.get(LONGITUDE));
            double longitude = Double.parseDouble(r.get(LATITUDE));

            int id = Integer.parseInt(r.get(ID));
            String intersection = r.get(INTERSECTION);
            boolean isSpeedCam = false;

            if (r.get(APPROACH_3).equals("SP")) {
                // Remove "(Speed Camera)"
                intersection = intersection.substring(0, intersection.length() - 14).trim();
                isSpeedCam = true;
            }

            Camera c = new Camera(latitude, longitude, id, intersection, isSpeedCam);
            cameras.add(c);
        }
    }
}
