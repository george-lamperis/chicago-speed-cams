






     

{

"redlight": [

 {

 "intid": "9042",
 "lat": "-87.52984826112849", 
 "lon": "41.70757690291348",
 "intersection": "10318 S Indianapolis (Speed Camera)",
 "fullname": "John Beniac-Park 499 Speed Camera 1",
 "golive": "Dec 24, 2013",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9129",
 "lat": "-87.7253827433152", 
 "lon": "41.86760272243294",
 "intersection": "1110 S Pulaski Rd (Speed Camera)",
 "fullname": "Frazier Magnet School Speed Camera 2",
 "golive": "Sep 5, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9041",
 "lat": "-87.70212117023745", 
 "lon": "41.901430911774796",
 "intersection": "1111 N Humboldt Blvd (Speed Camera)",
 "fullname": "Humboldt Park Speed Camera 2",
 "golive": "Nov 19, 2013",
 "approach1": "SB",
 "approach2": "NB",
 "approach3": "SP"
 },


 {

 "intid": "9045",
 "lat": "-87.6642476900556", 
 "lon": "41.69102545584918",
 "intersection": "11144 S Vincennes (Speed Camera)",
 "fullname": "Morgan Park Speed Camera 2",
 "golive": "Jan 8, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9044",
 "lat": "-87.66412238501842", 
 "lon": "41.690701951255015",
 "intersection": "11153 S Vincennes (Speed Camera)",
 "fullname": "Morgan Park Speed Camera 1",
 "golive": "Jan 8, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9128",
 "lat": "-87.72508368792016", 
 "lon": "41.867401834997956",
 "intersection": "1117 S Pulaski Rd (Speed Camera)",
 "fullname": "Frazier Magnet School Speed Camera 1",
 "golive": "Sep 5, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9031",
 "lat": "-87.65857252142276", 
 "lon": "41.95454086624301",
 "intersection": "1142 W Irving Park (Speed Camera)",
 "fullname": "Challenger Park Speed Camera 3",
 "golive": "Dec 6, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9053",
 "lat": "-87.66411469955516", 
 "lon": "41.883191521391076",
 "intersection": "115 N Ogden (Speed Camera)",
 "fullname": "Union Park Speed Camera 1",
 "golive": "Jan 25, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9077",
 "lat": "-87.6871949977686", 
 "lon": "41.90381710275111",
 "intersection": "1226 Western (Speed Camera)",
 "fullname": "Clemente Speed Camera 1",
 "golive": "Feb 24, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9078",
 "lat": "-87.68690274176198", 
 "lon": "41.90387807933294",
 "intersection": "1229 Western (Speed Camera)",
 "fullname": "Clemente Speed Camera 2",
 "golive": "Feb 24, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9100",
 "lat": "-87.6578607639445", 
 "lon": "41.793645091444645",
 "intersection": "1315 W Garfield Blvd (Speed Camera)",
 "fullname": "Sherman Park Speed Camera 2",
 "golive": "Mar 24, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9099",
 "lat": "-87.65870999891658", 
 "lon": "41.79422646635146",
 "intersection": "1334 W Garfield Blvd (Speed Camera)",
 "fullname": "Sherman Park Speed Camera 1",
 "golive": "Mar 24, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9116",
 "lat": "-87.6287812485988", 
 "lon": "41.89677912747405",
 "intersection": "14 W Chicago Ave (Speed Camera)",
 "fullname": "Francis Xavier School Speed Camera 3",
 "golive": "Apr 29, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9068",
 "lat": "-87.66704209415728", 
 "lon": "41.88451419700416",
 "intersection": "140 N Ashland (Speed Camera)",
 "fullname": "Union Park Speed Camera 3",
 "golive": "Feb 7, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9067",
 "lat": "-87.66674834697315", 
 "lon": "41.88451740794813",
 "intersection": "141 N Ashland (Speed Camera)",
 "fullname": "Union Park Speed Camera 2",
 "golive": "Feb 7, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9125",
 "lat": "-87.66281031843234", 
 "lon": "41.85250180995818",
 "intersection": "1440 W Cermak Rd (Speed Camera)",
 "fullname": "Benito Juarez  High School Speed Camera 1",
 "golive": "Sep 2, 2014",
 "approach1": "WB",
 "approach2": "EB",
 "approach3": "SP"
 },


 {

 "intid": "9135",
 "lat": "-87.66113775973551", 
 "lon": "41.742993034938884",
 "intersection": "1507 W 83rd St (Speed Camera)",
 "fullname": "Foster Park Speed Camera 1",
 "golive": "Oct 11, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9082",
 "lat": "-87.66757342998466", 
 "lon": "41.91168401599584",
 "intersection": "1635 N Ashland (Speed Camera)",
 "fullname": "Burr Speed Camera 1",
 "golive": "Mar 3, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9083",
 "lat": "-87.66787045896686", 
 "lon": "41.91179204452259",
 "intersection": "1638 N Ashland (Speed Camera)",
 "fullname": "Burr Speed Camera 2",
 "golive": "Mar 3, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9102",
 "lat": "-87.62892520294106", 
 "lon": "41.89577902516787",
 "intersection": "18 W Superior St (Speed Camera)",
 "fullname": "Francis Xavier Warde School Speed Camera 2",
 "golive": "Mar 28, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9115",
 "lat": "-87.62902590382873", 
 "lon": "41.89655610710888",
 "intersection": "19 W Chicago Ave (Speed Camera)",
 "fullname": "Francis Xavier School Speed Camera 2",
 "golive": "Apr 29, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9119",
 "lat": "-87.57851859043261", 
 "lon": "41.758740770124234",
 "intersection": "1901 E 75th St (Speed Camera)",
 "fullname": "Rosenblum Park Speed Camera 2",
 "golive": "Apr 30, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9008",
 "lat": "-87.67514359128938", 
 "lon": "41.82300132296577",
 "intersection": "2080 W Pershing Rd (Speed Camera)",
 "fullname": "McKinley Park Speed Camera 2",
 "golive": "Sep 7, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9114",
 "lat": "-87.6858278546488", 
 "lon": "41.853572622430164",
 "intersection": "2108 S Western Ave (Speed Camera)",
 "fullname": "Pickard School Speed Camera 4",
 "golive": "Apr 25, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9132",
 "lat": "-87.57290117806258", 
 "lon": "41.7369870911534",
 "intersection": "2109 E 87th St (Speed Camera)",
 "fullname": "Chicago Vocational HS Speed Camera 1",
 "golive": "Sep 18, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9113",
 "lat": "-87.68552968203949", 
 "lon": "41.85338493884992",
 "intersection": "2115 S Western Ave (Speed Camera)",
 "fullname": "Pickard School Speed Camera 3",
 "golive": "Apr 25, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9138",
 "lat": "-87.6197547203603", 
 "lon": "41.78005698693417",
 "intersection": "215 E 63rd St (Speed Camera)",
 "fullname": "Dulles Elementary School Speed Camera 1",
 "golive": "Sep 24, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9081",
 "lat": "-87.68433838912533", 
 "lon": "41.85215350507168",
 "intersection": "2326 W Cermak (Speed Camera)",
 "fullname": "Pickard Speed Camera 2",
 "golive": "Mar 3, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9079",
 "lat": "-87.68576187971952", 
 "lon": "41.90292940885378",
 "intersection": "2329 Division (Speed Camera)",
 "fullname": "Clemente Speed Camera 3",
 "golive": "Feb 24, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9080",
 "lat": "-87.68464766256305", 
 "lon": "41.85192952334165",
 "intersection": "2335 W Cermak (Speed Camera)",
 "fullname": "Pickard Speed Camera 1",
 "golive": "Mar 3, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9141",
 "lat": "-87.68239105927896", 
 "lon": "41.70648720051648",
 "intersection": "2416 W 103rd St (Speed Camera)",
 "fullname": "Beverly Park Speed Camera 1",
 "golive": "Oct 17, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9142",
 "lat": "-87.68239147238012", 
 "lon": "41.70626766073214",
 "intersection": "2417 W 103rd St (Speed Camera)",
 "fullname": "Beverly Park Speed Camera 2",
 "golive": "Oct 17, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9055",
 "lat": "-87.66832897708693", 
 "lon": "41.92620790295943",
 "intersection": "2432 N Ashland (Speed Camera)",
 "fullname": "Schaefer Park Speed Camera 2",
 "golive": "Jan 25, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9074",
 "lat": "-87.68589577919322", 
 "lon": "41.80123574797641",
 "intersection": "2440 W 51st St (Speed Camera)",
 "fullname": "Christopher Speed Camera 1",
 "golive": "Feb 24, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9054",
 "lat": "-87.66804246498943", 
 "lon": "41.926418831263994",
 "intersection": "2443 N Ashland (Speed Camera)",
 "fullname": "Schaefer Park Speed Camera 1",
 "golive": "Jan 25, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9075",
 "lat": "-87.68607060493738", 
 "lon": "41.801012880525555",
 "intersection": "2445 W 51st St (Speed Camera)",
 "fullname": "Christopher Speed Camera 2",
 "golive": "Feb 24, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9101",
 "lat": "-87.67093808722792", 
 "lon": "41.926153342916486",
 "intersection": "2448 N Clybourn Ave (Speed Camera)",
 "fullname": "Schaefer Park Speed Camera 1",
 "golive": "Mar 28, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9051",
 "lat": "-87.68724427195639", 
 "lon": "41.79370224937009",
 "intersection": "2513 W 55th St (Speed Camera)",
 "fullname": "Gage Park Speed Camera 3",
 "golive": "Jan 18, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9059",
 "lat": "-87.69054929251513", 
 "lon": "41.94661819673533",
 "intersection": "2549 W Addison (Speed Camera)",
 "fullname": "Lane Tech HS Speed Camera 3",
 "golive": "Jan 31, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9038",
 "lat": "-87.6874210628529", 
 "lon": "41.75021259302275",
 "intersection": "2550 W 79th (Speed Camera)",
 "fullname": "St Rita Speed Camera 4",
 "golive": "Dec 18, 2013",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9037",
 "lat": "-87.68798347502599", 
 "lon": "41.74985294026319",
 "intersection": "2603 W 79th (Speed Camera)",
 "fullname": "St Rita Speed Camera 3",
 "golive": "Jan 8, 2013",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9022",
 "lat": "-87.69620408862693", 
 "lon": "41.95387526461699",
 "intersection": "2705 W Irving Park (Speed Camera)",
 "fullname": "Horner Park Speed Camera 2",
 "golive": "Nov 15, 2013",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9023",
 "lat": "-87.6965965016668", 
 "lon": "41.954092657149786",
 "intersection": "2712 W Irving Park  (Speed Camera)",
 "fullname": "Horner Park Speed Camera 3",
 "golive": "Nov 15, 2013",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9021",
 "lat": "-87.69696403378589", 
 "lon": "41.96114938287925",
 "intersection": "2721 W Montrose (Speed Camera)",
 "fullname": "Horner Park Speed Camera 1",
 "golive": "Nov 15, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9013",
 "lat": "-87.69867209877323", 
 "lon": "41.86040786445197",
 "intersection": "2900 W Ogden Ave (Speed Camera)",
 "fullname": "Douglas Park Speed Camera 3",
 "golive": "Oct 17, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9009",
 "lat": "-87.69896237494204", 
 "lon": "41.86658528313424",
 "intersection": "2912 W Roosevelt Rd (Speed Camera)",
 "fullname": "Douglas Park Speed Camera 1",
 "golive": "Oct 5, 2013",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9010",
 "lat": "-87.69914296647906", 
 "lon": "41.866364024167034",
 "intersection": "2917 W Roosevelt (Speed Camera)",
 "fullname": "Douglas Park Speed Camera 2",
 "golive": "Oct 5, 2013",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9052",
 "lat": "-87.64649261049526", 
 "lon": "41.84081262592384",
 "intersection": "2928 S Halsted (Speed Camera)",
 "fullname": "McGuane Park Speed Camera 1",
 "golive": "Jan 18, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9011",
 "lat": "-87.70480112338926", 
 "lon": "41.9758840401294",
 "intersection": "3034 W Foster Ave (Speed Camera)",
 "fullname": "Legion Park Speed Camera 1",
 "golive": "Oct 5, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9094",
 "lat": "-87.7029526885659", 
 "lon": "41.877242773826396",
 "intersection": "3047 W Jackson Blvd (Speed Camera)",
 "fullname": "Horan Park Speed Camera 1",
 "golive": "Mar 17, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9014",
 "lat": "-87.70452234551168", 
 "lon": "41.89932514072566",
 "intersection": "3100 W Augusta Blvd (Speed Camera)",
 "fullname": "Humboldt Park Speed Camera 1",
 "golive": "Oct 17, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9139",
 "lat": "-87.66846320182256", 
 "lon": "41.938253305553516",
 "intersection": "3111 N Ashland Ave (Speed Camera)",
 "fullname": "Burley Elementary School Speed Camera 1",
 "golive": "Sep 26, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9085",
 "lat": "-87.78574089058974", 
 "lon": "41.93695084197675",
 "intersection": "3115 N Narragansett (Speed Camera)",
 "fullname": "Icci Academy Speed Camera 1",
 "golive": "Mar 10, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9086",
 "lat": "-87.78603672187927", 
 "lon": "41.93700435024341",
 "intersection": "3116 N Narragansett (Speed Camera)",
 "fullname": "Icci Academy Speed Camera 2",
 "golive": "Mar 10, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9140",
 "lat": "-87.66877877931208", 
 "lon": "41.93880546751142",
 "intersection": "3130 N Ashland Ave (Speed Camera)",
 "fullname": "Burley Elementary School Speed Camera 2",
 "golive": "Sep 26, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9043",
 "lat": "-87.70824462568225", 
 "lon": "41.9902819135942",
 "intersection": "3137 W Peterson (Speed Camera)",
 "fullname": "Legion Park Speed Camera 2",
 "golive": "Jan 3, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9109",
 "lat": "-87.61934752145963", 
 "lon": "41.89091009572781",
 "intersection": "319 E Illinois St (Speed Camera)",
 "fullname": "Ogden Plaza Park Speed Camera 1",
 "golive": "Apr 14, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9126",
 "lat": "-87.66879026653764", 
 "lon": "41.83595801848586",
 "intersection": "3200 S Archer Ave (Speed Camera)",
 "fullname": "Mulberry Park Speed Camera 1",
 "golive": "Sep 5, 2014",
 "approach1": "SB",
 "approach2": "NB",
 "approach3": "SP"
 },


 {

 "intid": "9144",
 "lat": "-87.7041703550325", 
 "lon": "41.793614646148406",
 "intersection": "3212 W 55th St (Speed Camera)",
 "fullname": "St. Gall Elementary School Speed Camera 2",
 "golive": "Jan 7, 2015",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9143",
 "lat": "-87.70434842139014", 
 "lon": "41.79339237207923",
 "intersection": "3217 W 55th St (Speed Camera)",
 "fullname": "St. Gall Elementary School Speed Camera 1",
 "golive": "Jan 7, 2015",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9104",
 "lat": "-87.72507433916897", 
 "lon": "41.93969900713657",
 "intersection": "3230 N Milwaukee Ave (Speed Camera)",
 "fullname": "Lorca School Speed Camera 1",
 "golive": "Apr 8, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9095",
 "lat": "-87.70614267856668", 
 "lon": "41.87659894232968",
 "intersection": "324 S Kedzie Ave (Speed Camera)",
 "fullname": "Horan Park Speed Camera 2",
 "golive": "Mar 17, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9118",
 "lat": "-87.6335862634599", 
 "lon": "41.756158269344546",
 "intersection": "341 W 76th St (Speed Camera)",
 "fullname": "Harvard School Speed Camera 3",
 "golive": "Apr 30, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9002",
 "lat": "-87.70972841983624", 
 "lon": "41.76439095689881",
 "intersection": "3450 W 71ST ST (Speed Camera)",
 "fullname": "Marquette Park Speed Camera 1",
 "golive": "Sep 6, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9117",
 "lat": "-87.6338165847716", 
 "lon": "41.7563743554102",
 "intersection": "346 W 76th St (Speed Camera)",
 "fullname": "Harvard School Speed Camera 2",
 "golive": "Apr 30, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9057",
 "lat": "-87.68807785258112", 
 "lon": "41.945574090874274",
 "intersection": "3521 N Western (Speed Camera)",
 "fullname": "Lane Tech HS Speed Camera 1",
 "golive": "Jan 31, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9058",
 "lat": "-87.68838396394528", 
 "lon": "41.9459625867886",
 "intersection": "3534 N Western (Speed Camera)",
 "fullname": "Lane Tech HS Speed Camera 2",
 "golive": "Jan 31, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9123",
 "lat": "-87.53756746533104", 
 "lon": "41.722818322421595",
 "intersection": "3535 E 95th St (Speed Camera)",
 "fullname": "Calumet Park Speed Camera 2",
 "golive": "Jun 30, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9124",
 "lat": "-87.5370407583455", 
 "lon": "41.72304461975978",
 "intersection": "3542 E 95th St (Speed Camera)",
 "fullname": "Calumet Park Speed Camera 3",
 "golive": "Jun 30, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9005",
 "lat": "-87.71789844857646", 
 "lon": "41.88093818643283",
 "intersection": "3646 W Madison St (Speed Camera)",
 "fullname": "Garfield Park Speed Camera 1",
 "golive": "Sep 10, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9006",
 "lat": "-87.71816831641307", 
 "lon": "41.87707082846323",
 "intersection": "3655 W Jackson Blvd (Speed Camera)",
 "fullname": "Garfield Park Speed Camera 2",
 "golive": "Sep 10, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9105",
 "lat": "-87.72264591078833", 
 "lon": "41.939040255975605",
 "intersection": "3809 W Belmont ave  (Speed Camera)",
 "fullname": "Lorca School Speed Camera 2",
 "golive": "Apr 8, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9106",
 "lat": "-87.72278142793657", 
 "lon": "41.93925823071868",
 "intersection": "3810 W Belmont Ave (Speed Camera)",
 "fullname": "Lorca School Speed Camera 3",
 "golive": "Apr 8, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9070",
 "lat": "-87.71959877920995", 
 "lon": "41.749715176851936",
 "intersection": "3832 W 79th St (Speed Camera)",
 "fullname": "Bogan HS Speed Camera 2",
 "golive": "Feb 10, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9007",
 "lat": "-87.6841140261617", 
 "lon": "41.82353114733556",
 "intersection": "3843 S Western Blvd (Speed Camera)",
 "fullname": "McKinley Park Speed Camera 1",
 "golive": "Sep 7, 2013",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9049",
 "lat": "-87.71721139909997", 
 "lon": "41.69120239624487",
 "intersection": "3843 W 111th (Speed Camera)",
 "fullname": "Chicago Agricultural HS Speed Camera 1",
 "golive": "Jan 13, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9069",
 "lat": "-87.71910467544157", 
 "lon": "41.74937090787529",
 "intersection": "3851 W 79th St (Speed Camera)",
 "fullname": "Bogan HS Speed Camera 1",
 "golive": "Feb 10, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9066",
 "lat": "-87.72479036232271", 
 "lon": "41.79330051203649",
 "intersection": "4040 W 55th (Speed Camera)",
 "fullname": "Hancock Speed Camera 4",
 "golive": "Feb 7, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9097",
 "lat": "-87.72765847088665", 
 "lon": "41.895448620893006",
 "intersection": "4040 W Chicago Ave (Speed Camera)",
 "fullname": "Orr HS Speed Camera 2",
 "golive": "Mar 24, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9096",
 "lat": "-87.72771533433409", 
 "lon": "41.89522798956752",
 "intersection": "4041 W Chicago Ave (Speed Camera)",
 "fullname": "Orr HS Speed Camera 1",
 "golive": "Mar 24, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9131",
 "lat": "-87.72687882014701", 
 "lon": "41.866257378192785",
 "intersection": "4042 W Roosevelt Rd (Speed Camera)",
 "fullname": "Frazier Magnet School Speed Camera 3",
 "golive": "Sep 9, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9065",
 "lat": "-87.72495665631904", 
 "lon": "41.79307966774864",
 "intersection": "4045 W 55th (Speed Camera)",
 "fullname": "Hancock Speed Camera 3",
 "golive": "Feb 7, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9103",
 "lat": "-87.76688350223966", 
 "lon": "41.954128259602165",
 "intersection": "4123 N Central Ave (Speed Camera)",
 "fullname": "Portage Park Speed Camera 2",
 "golive": "Mar 31, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9001",
 "lat": "-87.73166979484374", 
 "lon": "41.975501",
 "intersection": "4124 W Foster Ave (Speed Camera)",
 "fullname": "Gompers Park Speed Camera 1",
 "golive": "Aug 26, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9026",
 "lat": "-87.65551767517252", 
 "lon": "41.962596548691735",
 "intersection": "4429 N Broadway (Speed Camera)",
 "fullname": "Challenger Park Speed Camera1",
 "golive": "Nov 23, 2013",
 "approach1": "null",
 "approach2": "NB",
 "approach3": "SP"
 },


 {

 "intid": "9048",
 "lat": "-87.68456699562915", 
 "lon": "41.96226556022151",
 "intersection": "4432 N Lincoln (Speed Camera)",
 "fullname": "Welles Park Speed Camera 3",
 "golive": "Jan 4, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9046",
 "lat": "-87.68856455418805", 
 "lon": "41.96231378868856",
 "intersection": "4433 N Western (Speed Camera)",
 "fullname": "Welles Park Speed Camera 1",
 "golive": "Jan 4, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9047",
 "lat": "-87.68886227502836", 
 "lon": "41.9624218495406",
 "intersection": "4436 N Western (Speed Camera)",
 "fullname": "Welles Park Speed Camera 2",
 "golive": "Jan 4, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9027",
 "lat": "-87.6560244499908", 
 "lon": "41.96288063517309",
 "intersection": "4446 N Broadway (Speed Camera)",
 "fullname": "Challenger Park Speed Camera 2",
 "golive": "Nov 23, 2013",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9015",
 "lat": "-87.63373364696125", 
 "lon": "41.66317117223548",
 "intersection": "445 W 127th St (Speed Camera)",
 "fullname": "Major Taylor Speed Camera 1",
 "golive": "Oct 17, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9120",
 "lat": "-87.62017903427099", 
 "lon": "41.89003775504898",
 "intersection": "449 N Columbus Dr (Speed Camera)",
 "fullname": "Ogden Plaza Speed Cameras 2",
 "golive": "May 18, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9121",
 "lat": "-87.62041639513696", 
 "lon": "41.890122352505166",
 "intersection": "450 N Columbus Dr (Speed Camera)",
 "fullname": "Ogden Plaza Speed Cameras 3",
 "golive": "May 18, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9084",
 "lat": "-87.74312142256439", 
 "lon": "41.938988390759555",
 "intersection": "4620 W Belmont (Speed Camera)",
 "fullname": "Parsons Park Speed Camera 1",
 "golive": "Mar 10, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9093",
 "lat": "-87.7453343185986", 
 "lon": "41.99004267366519",
 "intersection": "4674 W Peterson Ave (Speed Camera)",
 "fullname": "Sauganash Speed Camera 3",
 "golive": "Mar 17, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9092",
 "lat": "-87.74622580384673", 
 "lon": "41.98980766908236",
 "intersection": "4707 W Peterson Ave (Speed Camera)",
 "fullname": "Sauganash Speed Camera 2",
 "golive": "Mar 17, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9134",
 "lat": "-87.74907897663441", 
 "lon": "41.96790232791309",
 "intersection": "4831 W Lawrence Ave (Speed Camera)",
 "fullname": "Ashmore Park Speed Camera 2",
 "golive": "Oct 11, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9050",
 "lat": "-87.74802266359677", 
 "lon": "41.92410632982652",
 "intersection": "4843 W Fullerton (Speed Camera)",
 "fullname": "St. Genevieve School Speed Camera 1",
 "golive": "Jan 13, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9133",
 "lat": "-87.7477237971432", 
 "lon": "41.970134735816295",
 "intersection": "4909 N Cicero Ave (Speed Camera)",
 "fullname": "Ashmore Park Speed Camera 1",
 "golive": "Sep 18, 2014",
 "approach1": "SB",
 "approach2": "NB",
 "approach3": "SP"
 },


 {

 "intid": "9062",
 "lat": "-87.72101713004055", 
 "lon": "41.80365647472294",
 "intersection": "4925 S Archer (Speed Camera)",
 "fullname": "Curie HS Speed Camera 3",
 "golive": "Jan 31, 2014",
 "approach1": "NEB",
 "approach2": "SWB",
 "approach3": "SP"
 },


 {

 "intid": "9060",
 "lat": "-87.72330350914287", 
 "lon": "41.80326489471",
 "intersection": "4929 S Pulaski (Speed Camera)",
 "fullname": "Curie HS Speed Camera 1",
 "golive": "Jan 31, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9076",
 "lat": "-87.68410655772436", 
 "lon": "41.802174029934044",
 "intersection": "5025 S Western (Speed Camera)",
 "fullname": "Christopher Speed Camera 3",
 "golive": "Feb 25, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9061",
 "lat": "-87.72354459803559", 
 "lon": "41.80140191299459",
 "intersection": "5030 S Pulaski (Speed Camera)",
 "fullname": "Curie HS Speed Camera 2",
 "golive": "Jan 31, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9108",
 "lat": "-87.7647619684764", 
 "lon": "41.87358141810132",
 "intersection": "506 S Central Ave (Speed Camera)",
 "fullname": "Columbus Park Speed Camera 3",
 "golive": "Apr 14, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9000",
 "lat": "-87.728215", 
 "lon": "41.97433265461055",
 "intersection": "5120 N Pulaski Rd (Speed Camera)",
 "fullname": "Gompers Park Speed Camera 2",
 "golive": "Aug 26, 2013",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9107",
 "lat": "-87.76446041521123", 
 "lon": "41.873329240103914",
 "intersection": "515 S Central Ave (Speed Camera)",
 "fullname": "Columbus Park Speed Camera 2",
 "golive": "Apr 14, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9012",
 "lat": "-87.60644477865196", 
 "lon": "41.79770388561477",
 "intersection": "5330 Cottage Grove (Speed Camera)",
 "fullname": "Washington Park Speed Camera 1",
 "golive": "Oct 5, 2013",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9030",
 "lat": "-87.6118759701359", 
 "lon": "41.793492645896656",
 "intersection": "536 E Morgan Dr (Speed Camera)",
 "fullname": "Washington Park Speed Camera 2",
 "golive": "Dec 6, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9089",
 "lat": "-87.65504097855228", 
 "lon": "41.795332774848234",
 "intersection": "5420 S Racine (Speed Camera)",
 "fullname": "Sherman Park Speed Camera 1",
 "golive": "Mar 10, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9064",
 "lat": "-87.72333321258739", 
 "lon": "41.7941545839503",
 "intersection": "5428 S Pulaski (Speed Camera)",
 "fullname": "Hancock Speed Camera 2",
 "golive": "Feb 7, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9071",
 "lat": "-87.76392864369929", 
 "lon": "41.96785573012919",
 "intersection": "5432 W Lawrence (Speed Camera)",
 "fullname": "Jefferson Park Speed Camera 1",
 "golive": "Feb 14, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9063",
 "lat": "-87.72303636259336", 
 "lon": "41.79404504596532",
 "intersection": "5433 S Pulaski (Speed Camera)",
 "fullname": "Hancock Speed Camera 1",
 "golive": "Feb 7, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9032",
 "lat": "-87.7623370579504", 
 "lon": "41.91823126606235",
 "intersection": "5440 W Grand (Speed Camera)",
 "fullname": "Prosser Speed Camera 1",
 "golive": "Dec 12, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9033",
 "lat": "-87.76299399881007", 
 "lon": "41.92412849125162",
 "intersection": "5446 W Fullerton (Speed Camera)",
 "fullname": "Prosser Speed Camera 2",
 "golive": "Dec 12, 2013",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9019",
 "lat": "-87.76426726425451", 
 "lon": "41.95332954454448",
 "intersection": "5454 W Irving Park (Speed Camera)",
 "fullname": "Portage Park Speed Camera 1",
 "golive": "Nov 10, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9072",
 "lat": "-87.76401945149458", 
 "lon": "41.96924714863934",
 "intersection": "5471 W Higgins (Speed Camera)",
 "fullname": "Jefferson Park Speed Camera 2",
 "golive": "Feb 14, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9034",
 "lat": "-87.76390557836874", 
 "lon": "41.92389747217995",
 "intersection": "5509 W Fullerton (Speed Camera)",
 "fullname": "Prosser Speed Camera 3",
 "golive": "Dec 12, 2013",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9025",
 "lat": "-87.68415735542791", 
 "lon": "41.792761119965434",
 "intersection": "5520 S Western (Speed Camera)",
 "fullname": "Gage Park Speed Camera 2",
 "golive": "Nov 23, 2013",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9024",
 "lat": "-87.68385704739886", 
 "lon": "41.79252199331773",
 "intersection": "5529 S Western (Speed Camera)",
 "fullname": "Gage Park Speed Camera 1",
 "golive": "Nov 23, 2013",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9145",
 "lat": "-87.70367640326066", 
 "lon": "41.79251999694851",
 "intersection": "5532 S Kedzie Ave (Speed Camera)",
 "fullname": "St. Gall Elementary School Speed Camera 3",
 "golive": "Jan 7, 2015",
 "approach1": "SB",
 "approach2": "NB",
 "approach3": "SP"
 },


 {

 "intid": "9017",
 "lat": "-87.62146289865515", 
 "lon": "41.72165713317433",
 "intersection": "57 E 95th St (Speed Camera)",
 "fullname": "Abbott Park Speed Camera 2",
 "golive": "Oct 12, 2013",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9130",
 "lat": "-87.7877408267013", 
 "lon": "41.98611938850496",
 "intersection": "5739 N Northwest Hwy (Speed Camera)",
 "fullname": "Taft High School Speed Camera 2",
 "golive": "Sep 9, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9018",
 "lat": "-87.7703695017756", 
 "lon": "41.87718940463045",
 "intersection": "5816 W Jackson (Speed Camera)",
 "fullname": "Columbus Park Speed Camera 1",
 "golive": "Nov 6, 2013",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9090",
 "lat": "-87.66845572473223", 
 "lon": "41.988904472466494",
 "intersection": "5885 N Ridge Ave (Speed Camera)",
 "fullname": "Senn Park Speed Camera 1",
 "golive": "Mar 18, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9091",
 "lat": "-87.74847678515228", 
 "lon": "41.9921194114274",
 "intersection": "6125 N Cicero Ave (Speed Camera)",
 "fullname": "Sauganash Speed Camera 1",
 "golive": "Mar 17, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9016",
 "lat": "-87.62135432849777", 
 "lon": "41.72187849819265",
 "intersection": "62 E 95th St (Speed Camera)",
 "fullname": "Abbott Park Speed Camera 1",
 "golive": "Oct 12, 2013",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9112",
 "lat": "-87.78282553773761", 
 "lon": "41.95307482555687",
 "intersection": "6226 W Irving Park Rd (Speed Camera)",
 "fullname": "Merrimac Park Speed Camera 1",
 "golive": "Apr 26, 2014",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "SP"
 },


 {

 "intid": "9039",
 "lat": "-87.78247948955652", 
 "lon": "41.9236145831186",
 "intersection": "6247 W Fullerton (Speed Camera)",
 "fullname": "Riis Park Speed Camera 1",
 "golive": "Dec 21, 2013",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9040",
 "lat": "-87.78264224184598", 
 "lon": "41.92383169923362",
 "intersection": "6250 W Fullerton (Speed Camera)",
 "fullname": "Riis Park Speed Camera 2",
 "golive": "Dec 21, 2013",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9028",
 "lat": "-87.62744787497319", 
 "lon": "41.873849598744286",
 "intersection": "629 State St (Speed Camera)",
 "fullname": "Jones HS Speed Camera 1",
 "golive": "Nov 25, 2013",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9029",
 "lat": "-87.62774075943143", 
 "lon": "41.87382420497741",
 "intersection": "630 State St (Speed Camera)",
 "fullname": "Jones HS Speed Camera 2",
 "golive": "Nov 25, 2013",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9146",
 "lat": "-87.616054", 
 "lon": "41.779286",
 "intersection": "6330 S Martin Luther King Dr (Speed Camera)",
 "fullname": "Dulles Elementary School Speed Camera 2",
 "golive": "Sep 24, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9087",
 "lat": "-87.78765548502582", 
 "lon": "41.938181258548575",
 "intersection": "6443 W Belmont (Speed Camera)",
 "fullname": "Icci Academy Speed Camera 3",
 "golive": "Mar 10, 2014",
 "approach1": "EB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9127",
 "lat": "-87.79083208818419", 
 "lon": "41.98299452666102",
 "intersection": "6510 W Bryn Mawr Ave (Speed Camera)",
 "fullname": "Taft High School Speed Camera 1",
 "golive": "Sep 5, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9088",
 "lat": "-87.78911871294291", 
 "lon": "41.93837715933998",
 "intersection": "6514 W Belmont (Speed Camera)",
 "fullname": "Icci Academy Speed Camera 4",
 "golive": "Mar 10, 2014",
 "approach1": "WB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9020",
 "lat": "-87.68980290863908", 
 "lon": "42.00025951144952",
 "intersection": "6523 N Western (Speed Camera)",
 "fullname": "Warren Park Speed Camera",
 "golive": "Nov 10, 2013",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9003",
 "lat": "-87.70305895795853", 
 "lon": "41.769106296454154",
 "intersection": "6818 S Kedzie Ave (Speed Camera)",
 "fullname": "Marquette Park Speed Camera 2",
 "golive": "Sep 6, 2013",
 "approach1": "SB",
 "approach2": " ",
 "approach3": "SP"
 },


 {

 "intid": "9004",
 "lat": "-87.70273790708362", 
 "lon": "41.76773186901884",
 "intersection": "6909 S Kedzie Ave (Speed Camera)",
 "fullname": "Marquette Park Speed Camera 3",
 "golive": "Sep 6, 2013",
 "approach1": "NB",
 "approach2": "         ",
 "approach3": "SP"
 },


 {

 "intid": "9098",
 "lat": "-87.72620452246953", 
 "lon": "41.89450330881178",
 "intersection": "732 N Pulaski Rd (Speed Camera)",
 "fullname": "Orr HS Speed Camera 3",
 "golive": "Mar 24, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9056",
 "lat": "-87.57630061252594", 
 "lon": "41.759441472271405",
 "intersection": "7422 S Jeffery (Speed Camera)",
 "fullname": "Rosenblum Park Speed Camera 1",
 "golive": "Jan 25, 2014",
 "approach1": "SB",
 "approach2": "NB",
 "approach3": "SP"
 },


 {

 "intid": "9073",
 "lat": "-87.63183332832696", 
 "lon": "41.75749703994495",
 "intersection": "7518 S Vincennes (Speed Camera)",
 "fullname": "Harvard Speed Camera 1",
 "golive": "Feb 17, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "9036",
 "lat": "-87.68306095859297", 
 "lon": "41.75271909284453",
 "intersection": "7738 S Western (Speed Camera)",
 "fullname": "St Rita Speed Camera 2",
 "golive": "Dec 18, 2013",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9035",
 "lat": "-87.68276525603093", 
 "lon": "41.752629293489285",
 "intersection": "7739 S Western (Speed Camera)",
 "fullname": "St Rita Speed Camera 1",
 "golive": "Dec 18, 2013",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9111",
 "lat": "-87.72209265589272", 
 "lon": "41.75054619772355",
 "intersection": "7826 S Pulaski Rd (Speed Camera)",
 "fullname": "Bogan HS Speed Camera 4",
 "golive": "Feb 14, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9110",
 "lat": "-87.72179453872008", 
 "lon": "41.75038452958284",
 "intersection": "7833 S Pulaski Rd (Speed Camera)",
 "fullname": "Bogan HS Speed Camera 3",
 "golive": "Feb 14, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9136",
 "lat": "-87.66338547998558", 
 "lon": "41.74247385456547",
 "intersection": "8318 S Ashland Ave (Speed Camera)",
 "fullname": "Foster Park Speed Camera 2",
 "golive": "Oct 11, 2014",
 "approach1": "SB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9137",
 "lat": "-87.66307332925098", 
 "lon": "41.741741651401504",
 "intersection": "8345 S Ashland Ave (Speed Camera)",
 "fullname": "Foster Park Speed Camera 3",
 "golive": "Oct 11, 2014",
 "approach1": "NB",
 "approach2": "null",
 "approach3": "SP"
 },


 {

 "intid": "9122",
 "lat": "-87.53543678735923", 
 "lon": "41.72075680558273",
 "intersection": "9618 S Ewing Ave (Speed Camera)",
 "fullname": "Calumet Park Speed Camera 1",
 "golive": "May 31, 2014",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "SP"
 },


 {

 "intid": "469",
 "lat": "-87.663794", 
 "lon": "41.76491",
 "intersection": "Ashland-71st",
 "fullname": "S ASHLAND AVE-W 71ST ST",
 "golive": "Oct 11, 2004",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "346",
 "lat": "-87.663063", 
 "lon": "41.735788",
 "intersection": "Ashland-87th",
 "fullname": "S ASHLAND AVE-W 87TH ST",
 "golive": "Mar 31, 2007",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "57",
 "lat": "-87.662684", 
 "lon": "41.721214",
 "intersection": "Ashland-95th",
 "fullname": "S ASHLAND AVE-W 95TH ST",
 "golive": "Jun 26, 2009",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "25",
 "lat": "-87.6678", 
 "lon": "41.916128",
 "intersection": "Ashland-Cortland",
 "fullname": "N ASHLAND AVE-W CORTLAND ST",
 "golive": "Feb 26, 2006",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1390",
 "lat": "-87.667496", 
 "lon": "41.903347",
 "intersection": "Ashland-Division",
 "fullname": "N ASHLAND AVE-W DIVISION ST",
 "golive": "Oct 25, 2007",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1380",
 "lat": "-87.668144", 
 "lon": "41.92515",
 "intersection": "Ashland-Fullerton",
 "fullname": "N ASHLAND AVE-W FULLERTON AVE",
 "golive": "Jun 8, 2009",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "8",
 "lat": "-87.669047", 
 "lon": "41.954291",
 "intersection": "Ashland-Irving Park",
 "fullname": "N ASHLAND AVE-W IRVING PARK RD",
 "golive": "Aug 26, 2008",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "284",
 "lat": "-87.66944", 
 "lon": "41.968876",
 "intersection": "Ashland-Lawrence",
 "fullname": "N ASHLAND AVE-W LAWRENCE AVE",
 "golive": "Aug 27, 2008",
 "approach1": "SB ",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1237",
 "lat": "-87.666802", 
 "lon": "41.881454",
 "intersection": "Ashland-Madison",
 "fullname": "N ASHLAND AVE-S ASHLAND AVE-W MADISON ST",
 "golive": "Mar 6, 2004",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "235",
 "lat": "-87.776504", 
 "lon": "41.945771",
 "intersection": "Austin-Addison",
 "fullname": "N AUSTIN AVE-W ADDISON ST",
 "golive": "Jun 28, 2008",
 "approach1": "EB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "233",
 "lat": "-87.775966", 
 "lon": "41.931139",
 "intersection": "Austin-Diversey",
 "fullname": "N AUSTIN AVE-W DIVERSEY AVE",
 "golive": "Jun 28, 2008",
 "approach1": "WB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1246",
 "lat": "-87.776775", 
 "lon": "41.953055",
 "intersection": "Austin-Irving Park",
 "fullname": "N AUSTIN AVE-W IRVING PARK RD",
 "golive": "Jun 30, 2008",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "351",
 "lat": "-87.659867", 
 "lon": "41.976323",
 "intersection": "Broadway-Foster",
 "fullname": "N BROADWAY -W FOSTER AVE",
 "golive": "Oct 18, 2007",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2823",
 "lat": "-87.660545", 
 "lon": "41.99819",
 "intersection": "Broadway-Sheridan-Devon",
 "fullname": "N BROADWAY -N SHERIDAN RD-W DEVON AVE-W SHERIDAN RD",
 "golive": "Oct 18, 2007",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1274",
 "lat": "-87.699643", 
 "lon": "41.997559",
 "intersection": "California-Devon",
 "fullname": "N CALIFORNIA AVE-W DEVON AVE",
 "golive": "Jul 31, 2008",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "325",
 "lat": "-87.697616", 
 "lon": "41.932096",
 "intersection": "California-Diversey",
 "fullname": "N CALIFORNIA AVE-W DIVERSEY AVE",
 "golive": "Jan 31, 2006",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "EB"
 },


 {

 "intid": "1267",
 "lat": "-87.69824", 
 "lon": "41.953973",
 "intersection": "California-Irving Park",
 "fullname": "N CALIFORNIA AVE-W IRVING PARK RD",
 "golive": "Aug 26, 2008",
 "approach1": "WB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "337",
 "lat": "-87.699401", 
 "lon": "41.990442",
 "intersection": "California-Peterson",
 "fullname": "N CALIFORNIA AVE-W PETERSON AVE",
 "golive": "Jun 29, 2009",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1279",
 "lat": "-87.639216", 
 "lon": "41.867252",
 "intersection": "Canal-Roosevelt",
 "fullname": "S CANAL ST-W ROOSEVELT RD",
 "golive": "Nov 30, 2008",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "314",
 "lat": "-87.766754", 
 "lon": "41.945877",
 "intersection": "Central-Addison",
 "fullname": "N CENTRAL AVE-W ADDISON ST",
 "golive": "Nov 15, 2010",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1300",
 "lat": "-87.766499", 
 "lon": "41.938573",
 "intersection": "Central-Belmont",
 "fullname": "N CENTRAL AVE-W BELMONT AVE",
 "golive": "Feb 5, 2009",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1296",
 "lat": "-87.765395", 
 "lon": "41.894852",
 "intersection": "Central-Chicago",
 "fullname": "N CENTRAL AVE-W CHICAGO AVE",
 "golive": "May 28, 2010",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1302",
 "lat": "-87.766248", 
 "lon": "41.931278",
 "intersection": "Central-Diversey",
 "fullname": "N CENTRAL AVE-W DIVERSEY AVE",
 "golive": "Aug 18, 2009",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1293",
 "lat": "-87.765989", 
 "lon": "41.92398",
 "intersection": "Central-Fullerton",
 "fullname": "N CENTRAL AVE-W FULLERTON AVE",
 "golive": "Oct 5, 2007",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1299",
 "lat": "-87.766998", 
 "lon": "41.953183",
 "intersection": "Central-Irving Park",
 "fullname": "N CENTRAL AVE-W IRVING PARK RD",
 "golive": "Jun 30, 2008",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1288",
 "lat": "-87.76512", 
 "lon": "41.887729",
 "intersection": "Central-Lake",
 "fullname": "N CENTRAL AVE-W LAKE ST",
 "golive": "Jul 17, 2009",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "390",
 "lat": "-87.743153", 
 "lon": "41.807623",
 "intersection": "Cicero-47th",
 "fullname": "S CICERO AVE-W 47TH ST",
 "golive": "Mar 31, 2008",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "97",
 "lat": "-87.747064", 
 "lon": "41.946124",
 "intersection": "Cicero-Addison",
 "fullname": "N CICERO AVE-W ADDISON ST",
 "golive": "Nov 30, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "142",
 "lat": "-87.742927", 
 "lon": "41.798659",
 "intersection": "Cicero-Archer",
 "fullname": "S ARCHER AVE-S CICERO AVE",
 "golive": "Nov 13, 2007",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "64",
 "lat": "-87.746111", 
 "lon": "41.916931",
 "intersection": "Cicero-Armitage",
 "fullname": "N CICERO AVE-W ARMITAGE AVE",
 "golive": "Nov 14, 2008",
 "approach1": "EB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "65",
 "lat": "-87.745805", 
 "lon": "41.895003",
 "intersection": "Cicero-Chicago",
 "fullname": "N CICERO AVE-W CHICAGO AVE",
 "golive": "Oct 5, 2007",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1335",
 "lat": "-87.746595", 
 "lon": "41.931533",
 "intersection": "Cicero-Diversey",
 "fullname": "N CICERO AVE-W DIVERSEY AVE",
 "golive": "Feb 11, 2010",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1329",
 "lat": "-87.746302", 
 "lon": "41.924237",
 "intersection": "Cicero-Fullerton",
 "fullname": "N CICERO AVE-W FULLERTON AVE",
 "golive": "Feb 25, 2006",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "68",
 "lat": "-87.745", 
 "lon": "41.873141",
 "intersection": "Cicero-Harrison",
 "fullname": "S CICERO AVE-W HARRISON ST",
 "golive": "Nov 19, 2009",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "204",
 "lat": "-87.74772", 
 "lon": "41.968029",
 "intersection": "Cicero-Lawrence",
 "fullname": "N CICERO AVE-W LAWRENCE AVE",
 "golive": "Nov 2, 2004",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "1331",
 "lat": "-87.746112", 
 "lon": "41.909626",
 "intersection": "Cicero-North",
 "fullname": "N CICERO AVE-W NORTH AVE",
 "golive": "May 31, 2008",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "225",
 "lat": "-87.748325", 
 "lon": "41.989898",
 "intersection": "Cicero-Peterson",
 "fullname": "N CICERO AVE-W PETERSON AVE",
 "golive": "Aug 26, 2008",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1020",
 "lat": "-87.745339", 
 "lon": "41.881815",
 "intersection": "Cicero-Washington",
 "fullname": "N CICERO AVE-W WASHINGTON BLVD",
 "golive": "Jun 14, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "332",
 "lat": "-87.631243", 
 "lon": "41.896635",
 "intersection": "Clark-Chicago",
 "fullname": "N CLARK ST-W CHICAGO AVE",
 "golive": "Oct 22, 2010",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1540",
 "lat": "-87.64048", 
 "lon": "41.925585",
 "intersection": "Clark-Fullerton",
 "fullname": "N CLARK ST-W FULLERTON PKWY",
 "golive": "Jun 27, 2007",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "1537",
 "lat": "-87.662248", 
 "lon": "41.954378",
 "intersection": "Clark-Irving Park",
 "fullname": "N CLARK ST-W IRVING PARK RD",
 "golive": "Jul 22, 2008",
 "approach1": "EB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "292",
 "lat": "-87.669985", 
 "lon": "41.989697",
 "intersection": "Clark-Ridge",
 "fullname": "N CLARK ST-N RIDGE AVE",
 "golive": "Jul 23, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "1575",
 "lat": "-87.605586", 
 "lon": "41.765868",
 "intersection": "Cottage Grove-71st-South Chicago",
 "fullname": "E 71ST ST-S COTTAGE GROVE AVE-S SOUTH CHICAGO AVE",
 "golive": "Apr 30, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "1593",
 "lat": "-87.673906", 
 "lon": "41.779315",
 "intersection": "Damen-63rd",
 "fullname": "S DAMEN AVE-W 63RD ST",
 "golive": "May 31, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "1620",
 "lat": "-87.678136", 
 "lon": "41.932285",
 "intersection": "Damen-Diversey-Clybourn",
 "fullname": "N CLYBOURN AVE-N DAMEN AVE-W DIVERSEY PKWY",
 "golive": "Jun 30, 2008",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1617",
 "lat": "-87.677258", 
 "lon": "41.903191",
 "intersection": "Damen-Division",
 "fullname": "N DAMEN AVE-W DIVISION AVE",
 "golive": "Dec 29, 2009",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2564",
 "lat": "-87.677898", 
 "lon": "41.925025",
 "intersection": "Damen-Fullerton-Elston",
 "fullname": "N DAMEN AVE-W FULLERTON AVE",
 "golive": "Feb 17, 2006",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2775",
 "lat": "-87.617465", 
 "lon": "41.838419",
 "intersection": "Dr Martin Luther King-31st",
 "fullname": "E 31ST ST-S DR MARTIN LUTHER KING JR DR",
 "golive": "Jun 22, 2008",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1448",
 "lat": "-87.708841", 
 "lon": "41.946622",
 "intersection": "Elston-Addison",
 "fullname": "N ELSTON AVE-W ADDISON ST",
 "golive": "Oct 19, 2007",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2214",
 "lat": "-87.719084", 
 "lon": "41.95373",
 "intersection": "Elston-Irving Park",
 "fullname": "N ELSTON AVE-W IRVING PARK RD",
 "golive": "Aug 12, 2009",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1450",
 "lat": "-87.740163", 
 "lon": "41.968108",
 "intersection": "Elston-Lawrence",
 "fullname": "N ELSTON AVE-W LAWRENCE AVE",
 "golive": "Jun 8, 2009",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1485",
 "lat": "-87.642833", 
 "lon": "41.706916",
 "intersection": "Halsted-103rd",
 "fullname": "S HALSTED ST-W 103RD ST",
 "golive": "Nov 30, 2008",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "183",
 "lat": "-87.642377", 
 "lon": "41.692355",
 "intersection": "Halsted-111th",
 "fullname": "S HALSTED ST-W 111TH ST",
 "golive": "Jan 1, 2006",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1484",
 "lat": "-87.642151", 
 "lon": "41.685081",
 "intersection": "Halsted-115th",
 "fullname": "S HALSTED ST-W 115TH ST",
 "golive": "May 29, 2008",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1483",
 "lat": "-87.641907", 
 "lon": "41.677815",
 "intersection": "Halsted-119th",
 "fullname": "S HALSTED ST-W 119TH ST",
 "golive": "Nov 2, 2004",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "66",
 "lat": "-87.644043", 
 "lon": "41.750643",
 "intersection": "Halsted-79th",
 "fullname": "S HALSTED ST-W 79TH ST",
 "golive": "Apr 30, 2008",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1489",
 "lat": "-87.643212", 
 "lon": "41.721458",
 "intersection": "Halsted-95th",
 "fullname": "S HALSTED ST-W 95TH ST",
 "golive": "Apr 30, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "1404",
 "lat": "-87.643004", 
 "lon": "41.714215",
 "intersection": "Halsted-99th",
 "fullname": "S HALSTED ST-W 99TH ST-W I57 HALSTED ST ER",
 "golive": "Jan 1, 2006",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "164",
 "lat": "-87.648094", 
 "lon": "41.903636",
 "intersection": "Halsted-Division",
 "fullname": "N HALSTED ST-W DIVISION ST",
 "golive": "Apr 7, 2004",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1524",
 "lat": "-87.64876", 
 "lon": "41.925454",
 "intersection": "Halsted-Fullerton-Lincoln",
 "fullname": "N HALSTED ST-N LINCOLN AVE-W FULLERTON AVE",
 "golive": "Aug 31, 2008",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "13",
 "lat": "-87.647361", 
 "lon": "41.881776",
 "intersection": "Halsted-Madison",
 "fullname": "N HALSTED ST-S HALSTED ST-W MADISON ST",
 "golive": "Jun 14, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "1221",
 "lat": "-87.648273", 
 "lon": "41.91092",
 "intersection": "Halsted-North",
 "fullname": "N HALSTED ST-W NORTH AVE",
 "golive": "Jun 29, 2009",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1514",
 "lat": "-87.646919", 
 "lon": "41.867152",
 "intersection": "Halsted-Roosevelt",
 "fullname": "S HALSTED ST-W ROOSEVELT RD",
 "golive": "Jun 14, 2008",
 "approach1": "WB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "152",
 "lat": "-87.720919", 
 "lon": "41.885203",
 "intersection": "Hamlin-Lake",
 "fullname": "N HAMLIN AVE-N HAMLIN BLVD-W LAKE ST",
 "golive": "May 31, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "1175",
 "lat": "-87.720752", 
 "lon": "41.880789",
 "intersection": "Hamlin-Madison",
 "fullname": "N HAMLIN AVE-W MADISON ST",
 "golive": "Apr 29, 2009",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "150",
 "lat": "-87.807006", 
 "lon": "41.945264",
 "intersection": "Harlem-Addison",
 "fullname": "N HARLEM AVE-W ADDISON ST",
 "golive": "Jun 21, 2009",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1793",
 "lat": "-87.806746", 
 "lon": "41.937997",
 "intersection": "Harlem-Belmont",
 "fullname": "N HARLEM AVE-W BELMONT AVE",
 "golive": "Jun 18, 2007",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1816",
 "lat": "-87.711879", 
 "lon": "41.910053",
 "intersection": "Homan-Kimball-North",
 "fullname": "N HOMAN AVE-N KIMBALL AVE-W NORTH AVE",
 "golive": "Oct 5, 2007",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "838",
 "lat": "-87.620224", 
 "lon": "41.891002",
 "intersection": "Illinois-Columbus",
 "fullname": "E ILLINOIS ST-N COLUMBUS DR",
 "golive": "Oct 28, 2010",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "1658",
 "lat": "-87.575353", 
 "lon": "41.722468",
 "intersection": "Jeffery-95th",
 "fullname": "E 95TH ST-S JEFFERY AVE",
 "golive": "Nov 26, 2007",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "23",
 "lat": "-87.705042", 
 "lon": "41.844518",
 "intersection": "Kedzie-26th",
 "fullname": "S KEDZIE AVE-W 26TH ST",
 "golive": "Nov 2, 2010",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "240",
 "lat": "-87.704823", 
 "lon": "41.837231",
 "intersection": "Kedzie-31st",
 "fullname": "S KEDZIE AVE-W 31ST ST",
 "golive": "May 23, 2008",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1685",
 "lat": "-87.703974", 
 "lon": "41.808115",
 "intersection": "Kedzie-47th",
 "fullname": "S KEDZIE AVE-W 47TH ST",
 "golive": "Mar 30, 2008",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1681",
 "lat": "-87.703553", 
 "lon": "41.793513",
 "intersection": "Kedzie-55th",
 "fullname": "S KEDZIE AVE-W 55TH ST",
 "golive": "Oct 20, 2009",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1679",
 "lat": "-87.703138", 
 "lon": "41.778972",
 "intersection": "Kedzie-63rd",
 "fullname": "S KEDZIE AVE-W 63RD ST",
 "golive": "Nov 26, 2007",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1678",
 "lat": "-87.702811", 
 "lon": "41.76439",
 "intersection": "Kedzie-71st",
 "fullname": "S KEDZIE AVE-W 71ST ST",
 "golive": "Sep 17, 2009",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2741",
 "lat": "-87.702396", 
 "lon": "41.749842",
 "intersection": "Kedzie-79th-Columbus",
 "fullname": "S KEDZIE AVE-W 79TH ST",
 "golive": "Jan 10, 2006",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1701",
 "lat": "-87.706965", 
 "lon": "41.917402",
 "intersection": "Kedzie-Armitage",
 "fullname": "N KEDZIE AVE-W ARMITAGE AVE",
 "golive": "Jan 23, 2009",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1384",
 "lat": "-87.707696", 
 "lon": "41.93933",
 "intersection": "Kedzie-Belmont",
 "fullname": "N KEDZIE AVE-N KENNEDY EXPY-W BELMONT AVE",
 "golive": "Mar 1, 2004",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "NB"
 },


 {

 "intid": "26",
 "lat": "-87.708016", 
 "lon": "41.95392",
 "intersection": "Kedzie-Irving Park",
 "fullname": "N KEDZIE AVE-W IRVING PARK RD",
 "golive": "Aug 26, 2008",
 "approach1": "WB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1744",
 "lat": "-87.745578", 
 "lon": "41.953446",
 "intersection": "Kilpatrick-Irving Park",
 "fullname": "N KILPATRICK AVE-W IRVING PARK RD",
 "golive": "Oct 21, 2008",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2780",
 "lat": "-87.736262", 
 "lon": "41.909754",
 "intersection": "Kostner-Grand-North",
 "fullname": "N KOSTNER AVE-W NORTH AVE",
 "golive": "Mar 11, 2004",
 "approach1": "EB",
 "approach2": "NB",
 "approach3": "SB"
 },


 {

 "intid": "217",
 "lat": "-87.734387", 
 "lon": "41.847697",
 "intersection": "Kostner-Ogden",
 "fullname": "S KOSTNER AVE-W OGDEN AVE",
 "golive": "May 23, 2008",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "509",
 "lat": "-87.734965", 
 "lon": "41.866031",
 "intersection": "Kostner-Roosevelt",
 "fullname": "S KOSTNER AVE-W ROOSEVELT RD",
 "golive": "Mar 18, 2004",
 "approach1": "EB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "102",
 "lat": "-87.632526", 
 "lon": "41.889187",
 "intersection": "LaSalle-Kinzie",
 "fullname": "N LA SALLE DR-N LA SALLE LOWER ST-W KINZIE ST",
 "golive": "Apr 30, 2004",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1974",
 "lat": "-87.625445", 
 "lon": "41.736271",
 "intersection": "Lafayette-87th",
 "fullname": "S LAFAYETTE ST-W 87TH ST",
 "golive": "Aug 31, 2009",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2788",
 "lat": "-87.638696", 
 "lon": "41.94024",
 "intersection": "Lake Shore-Belmont",
 "fullname": "N LAKE SHORE DR-W BELMONT AVE",
 "golive": "Jun 22, 2008",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2016",
 "lat": "-87.756147", 
 "lon": "41.924106",
 "intersection": "Laramie-Fullerton",
 "fullname": "N LARAMIE AVE-W FULLERTON AVE",
 "golive": "Jun 28, 2008",
 "approach1": "WB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "2018",
 "lat": "-87.757148", 
 "lon": "41.953299",
 "intersection": "Laramie-Irving Park",
 "fullname": "N LARAMIE AVE-W IRVING PARK RD",
 "golive": "Jan 27, 2010",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2003",
 "lat": "-87.755065", 
 "lon": "41.880371",
 "intersection": "Laramie-Madison",
 "fullname": "S LARAMIE AVE-W MADISON ST",
 "golive": "May 11, 2011",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2804",
 "lat": "-87.787444", 
 "lon": "41.997354",
 "intersection": "Milwaukee-Devon",
 "fullname": "N MILWUAKEE AVE-W DEVON AVE",
 "golive": "Feb 28, 2009",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "306",
 "lat": "-87.712251", 
 "lon": "41.931984",
 "intersection": "Milwaukee-Diversey",
 "fullname": "N MILWAUKEE AVE-W DIVERSEY AVE-N KIMBALL AVE",
 "golive": "Jun 29, 2009",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2207",
 "lat": "-87.754476", 
 "lon": "41.960632",
 "intersection": "Milwaukee-Montrose",
 "fullname": "N MILWAUKEE AVE-W MONTROSE AVE",
 "golive": "Mar 11, 2010",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2227",
 "lat": "-87.787872", 
 "lon": "41.975652",
 "intersection": "Nagle-Foster",
 "fullname": "N NAGLE AVE-W FOSTER AVE",
 "golive": "Oct 14, 2004",
 "approach1": "WB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "2236",
 "lat": "-87.785441", 
 "lon": "41.923676",
 "intersection": "Narragansett-Fullerton",
 "fullname": "N NARRAGANSETT AVE-W FULLERTON AVE",
 "golive": "Nov 14, 2008",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2240",
 "lat": "-87.786558", 
 "lon": "41.952916",
 "intersection": "Narragansett-Irving Park",
 "fullname": "N NARRAGANSETT AVE-W IRVING PARK RD",
 "golive": "Oct 14, 2004",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2075",
 "lat": "-87.795238", 
 "lon": "41.923599",
 "intersection": "Oak Park-Grand",
 "fullname": "N OAK PARK ST-W GRAND AVE",
 "golive": "Dec 15, 2009",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2126",
 "lat": "-87.72316", 
 "lon": "41.793205",
 "intersection": "Pulaski-55th",
 "fullname": "S PULASKI RD-W 55TH ST",
 "golive": "Nov 19, 2004",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "159",
 "lat": "-87.722759", 
 "lon": "41.778658",
 "intersection": "Pulaski-63rd",
 "fullname": "S PULASKI RD-W 63RD ST",
 "golive": "Mar 31, 2007",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "250",
 "lat": "-87.721913", 
 "lon": "41.749569",
 "intersection": "Pulaski-79th ",
 "fullname": "S PULASKI RD-W 79TH ST",
 "golive": "Sep 17, 2009",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2128",
 "lat": "-87.723422", 
 "lon": "41.802317",
 "intersection": "Pulaski-Archer-50th",
 "fullname": "S ARCHER AVE-S PULASKI RD-W 50TH ST",
 "golive": "Oct 31, 2007",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2148",
 "lat": "-87.726559", 
 "lon": "41.917165",
 "intersection": "Pulaski-Armitage",
 "fullname": "N PULASKI RD-W ARMITAGE AVE",
 "golive": "May 31, 2008",
 "approach1": "WB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "302",
 "lat": "-87.727185", 
 "lon": "41.939094",
 "intersection": "Pulaski-Belmont",
 "fullname": "N PULASKI RD-W BELMONT AVE",
 "golive": "May 19, 2007",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2136",
 "lat": "-87.724757", 
 "lon": "41.851546",
 "intersection": "Pulaski-Cermak",
 "fullname": "S PULASKI RD-W CERMAK RD",
 "golive": "Oct 11, 2004",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "69",
 "lat": "-87.726078", 
 "lon": "41.895367",
 "intersection": "Pulaski-Chicago",
 "fullname": "N PULASKI AVE-W CHICAGO AVE",
 "golive": "Feb 20, 2010",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2150",
 "lat": "-87.726979", 
 "lon": "41.931791",
 "intersection": "Pulaski-Diversey",
 "fullname": "N PULASKI RD-W DIVERSEY AVE",
 "golive": "May 15, 2009",
 "approach1": "SB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "2145",
 "lat": "-87.726303", 
 "lon": "41.902671",
 "intersection": "Pulaski-Division",
 "fullname": "N PULASKI RD-W DIVISION ST",
 "golive": "Jul 22, 2009",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "2157",
 "lat": "-87.728234", 
 "lon": "41.975532",
 "intersection": "Pulaski-Foster",
 "fullname": "N PULASKI RD-W FOSTER AVE",
 "golive": "Oct 18, 2007",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "11",
 "lat": "-87.726769", 
 "lon": "41.924484",
 "intersection": "Pulaski-Fullerton",
 "fullname": "N PULASKI RD-W FULLERTON AVE",
 "golive": "May 19, 2007",
 "approach1": "WB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "276",
 "lat": "-87.727617", 
 "lon": "41.953646",
 "intersection": "Pulaski-Irving Park",
 "fullname": "N PULASKI RD-W IRVING PARK RD",
 "golive": "May 19, 2009",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "77",
 "lat": "-87.728027", 
 "lon": "41.968242",
 "intersection": "Pulaski-Lawrence",
 "fullname": "N PULASKI RD-W LAWRENCE AVE",
 "golive": "Jul 31, 2008",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2147",
 "lat": "-87.72633", 
 "lon": "41.909886",
 "intersection": "Pulaski-North",
 "fullname": "N PULASKI RD-W NORTH AVE",
 "golive": "Nov 30, 2008",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "503",
 "lat": "-87.72864", 
 "lon": "41.990136",
 "intersection": "Pulaski-Peterson",
 "fullname": "N PULASKI RD-W PETERSON AVE",
 "golive": "Oct 30, 2008",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2140",
 "lat": "-87.725169", 
 "lon": "41.866172",
 "intersection": "Pulaski-Roosevelt",
 "fullname": "S PULASKI RD-W ROOSEVELT RD",
 "golive": "Jul 17, 2009",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1174",
 "lat": "-87.702455", 
 "lon": "41.895591",
 "intersection": "Sacramento-Chicago",
 "fullname": "N SACRAMENTO AVE-W CHICAGO AVE",
 "golive": "Sep 18, 2009",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2389",
 "lat": "-87.701291", 
 "lon": "41.884075",
 "intersection": "Sacramento-Lake",
 "fullname": "N SCARAMENTO AVE-W LAKE ST",
 "golive": "Oct 20, 2009",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "899",
 "lat": "-87.654993", 
 "lon": "41.976396",
 "intersection": "Sheridan-Foster",
 "fullname": "N SHERIDAN RD-W FOSTER AVE",
 "golive": "Jan 26, 2008",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2431",
 "lat": "-87.655242", 
 "lon": "41.985549",
 "intersection": "Sheridan-Hollywood",
 "fullname": "N SHERIDAN RD-W HOLLYWOOD AVE",
 "golive": "Oct 18, 2007",
 "approach1": "WB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2261",
 "lat": "-87.625374", 
 "lon": "41.780058",
 "intersection": "State-63rd",
 "fullname": "E 63RD ST-S STATE ST-W 63RD ST",
 "golive": "Jan 18, 2006",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "84",
 "lat": "-87.624757", 
 "lon": "41.75818",
 "intersection": "State-75th",
 "fullname": "E 75TH ST-S STATE ST-W 75TH ST",
 "golive": "Aug 28, 2009",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "1386",
 "lat": "-87.624547", 
 "lon": "41.750958",
 "intersection": "State-79th",
 "fullname": "S STATE ST-W 79TH ST",
 "golive": "Jun 26, 2009",
 "approach1": "WB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "156",
 "lat": "-87.58609", 
 "lon": "41.756944",
 "intersection": "Stony Island-76th",
 "fullname": "E 76TH ST-S STONY ISLAND AVE",
 "golive": "Nov 30, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "2299",
 "lat": "-87.585128", 
 "lon": "41.722338",
 "intersection": "Stony Island-95th",
 "fullname": "S STONY ISLAND AVE-E 95TH ST",
 "golive": "Oct 29, 2009",
 "approach1": "EB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "18",
 "lat": "-87.585823", 
 "lon": "41.751506",
 "intersection": "Stony Island-79th-South Chicago",
 "fullname": "E 79TH ST-S SOUTH CHICAGO AVE-S STONY ISLAND AVE",
 "golive": "May 7, 2007",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "NB"
 },


 {

 "intid": "2304",
 "lat": "-87.586295", 
 "lon": "41.77334",
 "intersection": "Stony Island-Cornell-67th",
 "fullname": "E 67TH ST-S STONY ISLAND AVE",
 "golive": "Nov 26, 2007",
 "approach1": "NB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "180",
 "lat": "-87.645174", 
 "lon": "41.73605",
 "intersection": "Vincennes-87th",
 "fullname": "S SUMMIT AVE-S VINCENNES AVE-W 87TH ST",
 "golive": "Nov 11, 2004",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "1412",
 "lat": "-87.630518", 
 "lon": "41.794201",
 "intersection": "Wentworth-Garfield",
 "fullname": "S WENTWORTH AVE-W GARFIELD BLVD",
 "golive": "May 8, 2008",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2473",
 "lat": "-87.685033", 
 "lon": "41.83028",
 "intersection": "Western-35th",
 "fullname": "S WESTERN AVE-W 35TH ST",
 "golive": "Oct 11, 2004",
 "approach1": "SB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "2470",
 "lat": "-87.684425", 
 "lon": "41.808426",
 "intersection": "Western-47th",
 "fullname": "S WESTERN AVE-W 47TH ST",
 "golive": "Nov 29, 2004",
 "approach1": "SB",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "2675",
 "lat": "-87.68404", 
 "lon": "41.793877",
 "intersection": "Western-55th",
 "fullname": "S WESTERN AVE-W GARFIELD BLVD",
 "golive": "Nov 1, 2003",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "19",
 "lat": "-87.683638", 
 "lon": "41.779214",
 "intersection": "Western-63rd",
 "fullname": "S WESTERN AVE-W 63RD ST",
 "golive": "Nov 12, 2007",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2464",
 "lat": "-87.683242", 
 "lon": "41.764703",
 "intersection": "Western-71st",
 "fullname": "S WESTERN AVE-W 71ST ST",
 "golive": "Dec 29, 2009",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "47",
 "lat": "-87.682847", 
 "lon": "41.750101",
 "intersection": "Western-79th",
 "fullname": "S WESTERN AVE-W 79TH ST",
 "golive": "May 7, 2007",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2494",
 "lat": "-87.688258", 
 "lon": "41.946731",
 "intersection": "Western-Addison",
 "fullname": "N WESTERN AVE-W ADDISON ST",
 "golive": "Jun 8, 2009",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2475",
 "lat": "-87.685639", 
 "lon": "41.852031",
 "intersection": "Western-Cermak",
 "fullname": "S WESTERN AVE-W CERMAK RD",
 "golive": "Dec 29, 2009",
 "approach1": "SB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2485",
 "lat": "-87.686843", 
 "lon": "41.895743",
 "intersection": "Western-Chicago",
 "fullname": "N WESTERN AVE-W CHICAGO AVE",
 "golive": "May 7, 2007",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "285",
 "lat": "-87.689881", 
 "lon": "41.99773",
 "intersection": "Western-Devon",
 "fullname": "N WESTERN AVE-W DEVON AVE",
 "golive": "Jul 31, 2008",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "308",
 "lat": "-87.687883", 
 "lon": "41.932179",
 "intersection": "Western-Diversey-Elston",
 "fullname": "N ELSTON AVE-N WESTERN AVE-W DIVERSEY AVE",
 "golive": "Jul 18, 2008",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "264",
 "lat": "-87.689148", 
 "lon": "41.97589",
 "intersection": "Western-Foster",
 "fullname": "N WESTERN AVE-W FOSTER AVE",
 "golive": "Jun 5, 2007",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "54",
 "lat": "-87.687641", 
 "lon": "41.9249",
 "intersection": "Western-Fullerton",
 "fullname": "N WESTERN AVE-W FULLERTON AVE",
 "golive": "Jun 29, 2009",
 "approach1": "NB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "5",
 "lat": "-87.688906", 
 "lon": "41.968614",
 "intersection": "Western-Lawrence",
 "fullname": "N WESTERN AVE-W LAWRENCE AVE",
 "golive": "Jul 31, 2008",
 "approach1": "SB ",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "71",
 "lat": "-87.686432", 
 "lon": "41.881161",
 "intersection": "Western-Madison",
 "fullname": "N WESTERN AVE-S WESTERN AVE-W MADISON ST",
 "golive": "Jun 1, 2004",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2465",
 "lat": "-87.683443", 
 "lon": "41.771977",
 "intersection": "Western-Marquette",
 "fullname": "S WESTERN AVE-W MARQUETTE RD",
 "golive": "Nov 30, 2008",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2496",
 "lat": "-87.688681", 
 "lon": "41.961313",
 "intersection": "Western-Montrose",
 "fullname": "N WESTERN AVE-W MONTROSE AVE",
 "golive": "Aug 26, 2008",
 "approach1": "NB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "273",
 "lat": "-87.687222", 
 "lon": "41.91032",
 "intersection": "Western-North",
 "fullname": "N WESTERN AVE-W NORTH AVE",
 "golive": "Nov 26, 2007",
 "approach1": "NB",
 "approach2": "EB",
 "approach3": "null"
 },


 {

 "intid": "2472",
 "lat": "-87.684249", 
 "lon": "41.822906",
 "intersection": "Western-Pershing",
 "fullname": "S WESTERN BLVD-W PERSHING RD",
 "golive": "May 15, 2008",
 "approach1": "SB",
 "approach2": "WB",
 "approach3": "null"
 },


 {

 "intid": "2502",
 "lat": "-87.689671", 
 "lon": "41.9905",
 "intersection": "Western-Peterson",
 "fullname": "N WESTERN AVE-W PETERSON AVE",
 "golive": "Feb 26, 2004",
 "approach1": "WB",
 "approach2": "SB",
 "approach3": "null"
 },


 {

 "intid": "90",
 "lat": "-87.690246", 
 "lon": "42.012279",
 "intersection": "Western-Touhy",
 "fullname": "N WESTERN AVE-W TOUHY AVE",
 "golive": "Oct 21, 2008",
 "approach1": "SB ",
 "approach2": "NB",
 "approach3": "null"
 },


 {

 "intid": "2480",
 "lat": "-87.686271", 
 "lon": "41.876083",
 "intersection": "Western-Van Buren",
 "fullname": "S WESTERN AVE-W VAN BUREN ST",
 "golive": "May 29, 2008",
 "approach1": "WB",
 "approach2": "SB",
 "approach3": "null"
 }


]

}
